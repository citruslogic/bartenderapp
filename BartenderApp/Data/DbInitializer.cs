﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BartenderApp.Models;

namespace BartenderApp.Data
{
    public class DbInitializer
    {
        public static void Initialize(BartenderContext context)
        {
            context.Database.EnsureCreated();

            // Look for any customers.
            if (context.Customers.Any())
            {
                return;   // DB has been seeded
            }

            var servers = new Server[]
            {
                new Server{ServerName="Brian Kohler"}
            };

            foreach (Server s in servers)
            {
                context.Servers.Add(s);
            }

            context.SaveChanges();

            var customers = new Customer[]
            {
                new Customer{ CustomerID=Customer.GetCustomerID(), IsTab=false },

            };

            foreach (Customer c in customers)
            {
                context.Customers.Add(c);
            }

            context.SaveChanges();

            var drinks = new Drink[]
            {
                new Drink{ DrinkName="Margarita", DrinkPrice=6.00m},
                new Drink{ DrinkName="Mai Tai", DrinkPrice=6.00m},
                new Drink{ DrinkName="Screwdriver", DrinkPrice=5.00m}
            };

            foreach (Drink d in drinks)
            {
                context.Drinks.Add(d);
            }

            context.SaveChanges();

            var orders = new Order[]
            {
                new Order{ CustomerID=Customer.GetCustomerID(), DrinkID=1 }
            };
            
            foreach (Order o in orders)
            {
                context.Orders.Add(o);
            }

            context.SaveChanges();


        }
    }


}
