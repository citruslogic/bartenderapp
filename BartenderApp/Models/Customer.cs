﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net;
using System.Net.Sockets;

namespace BartenderApp.Models
{
    public class Customer
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CustomerID { get; set; }
        [Display(Name = "On Tab")]
        public Boolean IsTab { get; set; }


        /*  Creates a CustomerID from the host number of the machine we're running on. 
         *  Assuming a single customer is using the kiosk, and the host number becomes
         *  the table number. Also assumes static IP addressing is used. Ideally, table 
         *  kiosk addresses are reserved on their own subnet. 
         */
        public static int GetCustomerID()
        {
            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                // Network is unavailable, so throw an exception.
                throw new Exception("Network is not available and is required for kiosk operation.");
            }

            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());

            int idx = host
                .AddressList
                .FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork).ToString().LastIndexOf('.');

            return Int32.Parse(
                host
                .AddressList
                .FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork).ToString().Substring(idx + 1));
        }
    }
}
