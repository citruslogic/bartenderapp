﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BartenderApp.Models
{
    public class Order
    {
        [Key]
        public int OrderID { get; set; }
        public int DrinkID { get; set; }
        public int CustomerID { get; set; }

        public ICollection<Customer> Customers { get; set; }

    }
}
