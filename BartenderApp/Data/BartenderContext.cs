﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BartenderApp.Models;
using Microsoft.EntityFrameworkCore;

namespace BartenderApp.Data
{
    public class BartenderContext : DbContext
    {
        public BartenderContext(DbContextOptions<BartenderContext> options) : base(options)
        {
        }


        public DbSet<Drink> Drinks { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Server> Servers { get; set; }
        public DbSet<Order> Orders { get; set; }
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Drink>().ToTable("Drink");
            modelBuilder.Entity<Customer>().ToTable("Customer");
            modelBuilder.Entity<Server>().ToTable("Server");
            modelBuilder.Entity<Order>().ToTable("Order");


        }

    }
}
