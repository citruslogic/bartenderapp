﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BartenderApp.Models
{
    public class Server
    {
        [Key]
        public int ServerID { get; set; }
        [Display(Name = "Server Name")]
        public string ServerName { get; set; }

        [Display(Name = "Orders")]
        public ICollection<Order> Orders { get; set; }
     

    }
}
