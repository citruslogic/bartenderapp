﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BartenderApp.Models
{
    public class Drink
    {
        [Key]
        public int DrinkID { get; set; }
        [Display(Name = "Drink Name")]
        public string DrinkName { get; set; }
        [Display(Name = "Drink Price")]
        public decimal DrinkPrice { get; set; }

    }
}
